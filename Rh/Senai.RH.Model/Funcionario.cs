﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rh.Senai.RH.Model
{
    class Funcionario
    {
        private long id;

        public long ID
        {
            get { return id; }
            set { id = value; }
        } // FIM

        private string nome;

        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        } // FIM

        private string cpf;

        public string Cpf
        {
            get { return cpf; }
            set { cpf = value; }
        } // FIM

        private string rg;

        public string Rg
        {
            get { return rg; }
            set { rg = value; }
        } // FIM

        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; }
        } // FIM

        private string telefone;

        public string Telefone
        {
            get { return telefone; }
            set { telefone = value; }
        } // FIM

        // Construtor 
        public Funcionario()
        {

        }

        // SOBRESCREVERO MÉTODO ToString()
        public override string ToString()
        {
            return this.nome;
        }

        public Funcionario(long id, string nome, string cpf, string rg, string email, string telefone)
        {
            this.id = id;
            this.nome = nome;
            this.cpf = cpf;
            this.rg = rg;
            this.email = email;
            this.telefone = telefone;
        }
    }
}
