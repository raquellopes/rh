﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rh.Senai.RH.Model
{
    class Dependencia
    {
        private long id;

        public long ID
        {
            get { return id; }
            set { id = value; }
        } // FIM

        private string descricao;

        public string Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        } // FIM

        // SOBRESCREVERO MÉTODO ToString()
        public override string ToString()
        {
            return this.descricao;
        }

        public Dependencia()
        {

        } // CONSTRUTOR VAZIO


        public Dependencia(long id, string descricao)
        {
            this.id = id;
            this.descricao = descricao;
        } // CONSTRUTOR
    }
}
