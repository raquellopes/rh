﻿using Rh.Senai.RH.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rh.Senai.RH.Dao
{
    class DependenciaDao : IDao<Dependencia>
    {
        // ATRIBUTOS 
        // CONEXÃO COM BANCO
        private SqlConnection connection;

        // INSTRUÇÃO SQL
        private string sql = null;

        // MENSAGEM DO MESSAGEBOX
        private string msg = null;

        // TITULO DA MENSAGEM
        private string titulo = null;

        public DependenciaDao()
        {
            // CRIA UMA CONEXÃO COM BANCO DE DADOS
            connection = new ConnectionFactorys().GetConnection();
        } // CONSTRUTOR

        public List<Dependencia> Consultar()
        {
            // COMANDO DE CONSULTA SQL
            sql = "SELECT * FROM Dependencia";

            // LISTA DE FUNCIONARIO CADRASTRADORS
            List<Dependencia> dependentes = new List<Dependencia>();

            try
            {
                // ABRE CONEXÃO COM O BANCO 
                connection.Open();

                // COMANDO SQL
                SqlCommand cm = new SqlCommand(sql, connection);

                // CRIAR UM LEITOR DE DADOS
                SqlDataReader leitor = cm.ExecuteReader();

                while (leitor.Read())
                {
                    Dependencia dependencia = new Dependencia();
                    dependencia.ID = (long)leitor["IDDependencia"];
                    dependencia.Descricao = leitor["Descricao"].ToString();

                    // ADICIONAR O DEPENDENTENCIA NA LISTA DE DEPENDENTES
                    dependentes.Add(dependencia);
                }
            }
            catch (Exception ex)
            {
                msg = "Erro ao consultar os dependentes cadrastrador" + ex.Message;
                titulo = "Erro ..";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                connection.Close();
            }
            // RETORNA LISTA DO SQL
            return dependentes;

        } // FIM CONSULTAR

        public Dependencia Consultar(string parametro)
        {
            throw new NotImplementedException();
        }

        public void Excluir(Dependencia dependencia)
        {
            // ABRE A CONEXÃO COM BANCO DE DADOS
            connection.Open();

            // COMANDO SQL
            sql = "DELETE FROM Dependencia WHERE IDDependencia = @IDDependencia";

            try
            {
                // CRIAR COMANDO SQL
                SqlCommand cmd = new SqlCommand(sql,connection);

                // ADICIONAR VALOR AO PARAMETRO
                cmd.Parameters.AddWithValue("@IDDependencia", dependencia.ID);

                // EXECUTAR O COMANDO SQL NO BANCO DE DADOS
                cmd.ExecuteNonQuery();

                // MENSAGEM 
                msg = "Executado com sucesso";
                titulo = "Sucesso ...";
                MessageBox.Show(msg,titulo,MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            catch (SqlException ex)
            {
                msg = "Erro ao excluir funcionario"+ex.Message;
                titulo = "Erro ...";
                MessageBox.Show(msg,titulo,MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            finally
            {
                connection.Close();
            }
        }








        public void Salvar(Dependencia dependencia)
        {

                sql = "INSERT INTO Dependencia(Descricao) VALUES (@Descricao)";
            

            try
            {
                // ABRIR CONEXÃO COM OBANCO 
                connection.Open();

                SqlCommand cm = new SqlCommand(sql, connection);
                cm.Parameters.AddWithValue("@Descricao", dependencia.Descricao);

                // INSERT 
                cm.ExecuteNonQuery();
                msg = "Executado com sucesso!";
                titulo = "...";
                MessageBox.Show(msg,titulo,MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            catch (SqlException ex)
            {
                msg = "Erro"+ex.Message;
                titulo = "Erro ...";
                MessageBox.Show(msg,titulo,MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
