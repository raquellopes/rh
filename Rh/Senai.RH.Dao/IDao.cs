﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rh.Senai.RH.Dao
{
    interface IDao<T> //Tipo generico
    {
        /// <summary>
        /// Salvar tipo
        /// </summary>
        /// <param name="t">Típo</param>
        void Salvar(T t);
        List<T> Consultar();
        void Excluir(T t);
        T Consultar(string parametro);
    }
}
