﻿
using Rh.Senai.RH.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rh.Senai.RH.Dao
{
    class DependenteDao : IDao<Dependente>
    {
        // CONEXÃO COM BANCO DE DADOS
        private SqlConnection connection;

        //  INSTRUÇÃO SQL
        private string sql = null;

        // INFO MENSAGEM
        private string msg = null;
        private string titulo = null;

        public DependenteDao()
        {
            connection = new ConnectionFactorys().GetConnection();
        }

        public List<Dependente> Consultar()
        {
            // INSTRUÇÃO SQL
            sql = "SELECT * FROM DependenteView";

            // LISTA DE DEPENDENTE
            List<Dependente> dependentes = new List<Dependente>();

            try
            {
                // ABRE CONEXÃO COM O BANCO 
                connection.Open();

                // CRIA UM COMANDO SQL
                SqlCommand cmd = new SqlCommand(sql, connection);

                // CRIA UM LEITOR PARA RECEBER OS DADOS DA TABELA
                SqlDataReader leitor = cmd.ExecuteReader();

                // ENQUANTO O LEITOR ESIVER LENDO 
                while (leitor.Read())
                {
                    // CRIA UMA INSTANCIA DE DEPENDENCIA
                    Dependencia dependencia = new Dependencia();

                    // CRIA UMA INSTANCIA DE FUNCIONARIO
                    Funcionario funcionario = new Funcionario();

                    // DEFINE OS DADOS DE FUNCIONARIO 
                    funcionario.ID = (long)leitor["IDFuncionario"];
                    funcionario.Nome = leitor["NomeFuncionario"].ToString();
                    funcionario.Cpf = leitor["CPDFuncionario"].ToString();
                    funcionario.Rg = leitor["Rg"].ToString();
                    funcionario.Email = leitor["Email"].ToString();
                    funcionario.Telefone = leitor["Telefone"].ToString();

                    // DEFINE OS DADOS DE DEPENDENCIA
                    dependencia.ID = (long)leitor["IDDependencia"];
                    dependencia.Descricao = leitor["Descricao"].ToString();

                    Dependente dependente = new Dependente();
                    dependente.ID = (long)leitor["IDDependente"];
                    dependente.Nome = leitor["NomeDependente"].ToString();
                    dependente.Cpf = leitor["CPFDependente"].ToString();
                    dependente.DataNascimento = (DateTime)leitor["DataNascimento"];
                    dependente.Dependencia = dependencia;
                    dependente.Funcionario = funcionario;

                    dependentes.Add(dependente);
                }
            }
            catch (SqlException ex)
            {
                msg = "Erro ao consultar os dependentes "+ex.Message;
                titulo = "Erro";
                MessageBox.Show(msg,titulo,MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            finally
            {
                connection.Close();
            }
            return dependentes;
        } // FIM

        public Dependente Consultar(string parametro)
        {
            throw new NotImplementedException();
        }

        public void Excluir(Dependente dependente)
        {
            sql = "DELETE FROM Dependente WHERE IDDependente = @IDDependente";

            try
            {
                // ABRE A CONEXÃO COM O BANCO DE DADOS
                connection.Open();

                // CRIA UM COMANDO SQL
                SqlCommand cmd = new SqlCommand(sql, connection);

                // ADICIONAR VALOR AO PARAMETRO @IDDpendente
                cmd.Parameters.AddWithValue("@IDDependente", dependente.ID);

                // EXECUTA O COMANDO NO SQL NO BANCO DE DADOS
                cmd.ExecuteNonQuery();

                // MENSAGEM DE SUCESSO
                msg = "Dependente excluido com sucesso!";
                titulo = "Sucesso";
                MessageBox.Show(msg,titulo,MessageBoxButtons.OK,MessageBoxIcon.Information);

            }
            catch (SqlException ex)
            {
                msg = "Erro ao excluir funcionario "+ex.Message;
                titulo = "Erro";
                MessageBox.Show(msg,titulo,MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            finally
            {
                connection.Close();
            }
        }

        public void Salvar(Dependente dependete)
        {
            // VERIFICAR SE O ID DO DEPENDENTE É DIFERENTE DE 0
            if (dependete.ID != 0)
            {
                sql = "UPDATE Dependente SET  Nome = @Nome, Cpf = @Cpf, DataNascimento = @DataNascimento, IDFuncionario = @IDFuncionario, IDDependencia = @IDDependencia WHERE IDDependente = @IDDependente";

            }
            else
            {
                sql = "INSERT INTO dependente (Nome, Cpf, DataNascimento, IDFuncionario, IDDependencia) VALUES (@Nome, @Cpf, @DataNascimento, @IDFuncionario, @IDDependencia)";
            }

            try
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.Parameters.AddWithValue("@Nome", dependete.Nome);
                cmd.Parameters.AddWithValue("@Cpf", dependete.Cpf);
                cmd.Parameters.AddWithValue("@DataNascimento", dependete.DataNascimento);
                cmd.Parameters.AddWithValue("@IDFuncionario", dependete.Funcionario.ID);
                cmd.Parameters.AddWithValue("@IDDependencia", dependete.Dependencia.ID);
                cmd.Parameters.AddWithValue("@IDDependente" ,dependete.ID);
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                msg = "Erro ao salvar dependente "+ex.Message;
                titulo = "Erro";
                MessageBox.Show(msg,titulo,MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
