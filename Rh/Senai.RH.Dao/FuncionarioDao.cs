﻿using Rh.Senai.RH.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rh.Senai.RH.Dao
{
    class FuncionarioDao : IDao<Funcionario>
    {
        // atributos
        // conexão com banco de dados
        private SqlConnection connection;

        // instrução sql
        private string sql = null;

        // mensagem do messagebox
        private string msg = null;

        // titulo do messagebox
        private string titulo = null;

        // metodos
        public FuncionarioDao()
        {
            // cria uma conexão com o banco de dados
            connection = new ConnectionFactorys().GetConnection();
        }

        public Funcionario Consultar(string parametro)
        {
            // INSTRUÇÃO SQL
            sql = "SELECT * FROM Funcionario WHERE Cpf = @Cpf";

            Funcionario funcionario = null;
            try
            {
                // ABRE A CONEXÃO COM O BANCO DE DADOS
                connection.Open();

                // COMANDO SQL
                SqlCommand cmd = new SqlCommand(sql, connection);

                // PARAMETRO DO COMANDO SQL
                cmd.Parameters.AddWithValue("@Cpf", parametro);

                // LEITOR DE DADOS SQL
                // RECEBE OS DADOS DO BANCO DE DADOS
                SqlDataReader leitor = cmd.ExecuteReader();

                while (leitor.Read())
                {

                    // COMPARAR SE O CPF DOS REGRISTROS É IGUAL AO CPF DE PARAMETRO

                    if (parametro.Equals(leitor["Cpf"].ToString()))
                    {
                        // SE FOR VERDADEIRA
                        // CRIA O FUNCIONARIO
                        funcionario = new Funcionario();
                        funcionario.ID = (long)leitor["IDFuncionario"];
                        funcionario.Nome = leitor["Nome"].ToString();
                        funcionario.Cpf = leitor["Cpf"].ToString();
                        funcionario.Rg = leitor["Rg"].ToString();
                        funcionario.Email = leitor["Email"].ToString();
                        funcionario.Telefone = leitor["Telefone"].ToString();
                    }
                }
                
            } 

            catch (SqlException ex)
            {

                msg = "Erro ao consultar o funcionario";
                titulo = "Erro ...";
                MessageBox.Show(msg,titulo,MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            finally
            {
                connection.Close();
            }
            return funcionario;
        }

        public List<Funcionario> Consultar()
        {
            // Comando sql consulta
            sql = "SELECT * FROM Funcionario";
            
            // Lista de funcionarios cadrastrados
            List<Funcionario> funcionarios = new List<Funcionario>();

            try
            {
                // Abre a conexão com o banco de daddos 
                connection.Open();

                // Comando sql
                SqlCommand cmd = new SqlCommand(sql, connection);

                // Criar um leitor de dados
                SqlDataReader leitor = cmd.ExecuteReader();

                // Enquanto o leitor tiver dados para ler
                while (leitor.Read())
                {
                    Funcionario funcionario = new Funcionario();
                    funcionario.ID = (long) leitor["IDFuncionario"];
                    funcionario.Nome = leitor["Nome"].ToString();
                    funcionario.Cpf = leitor["Cpf"].ToString();
                    funcionario.Rg = leitor["Rg"].ToString();
                    funcionario.Email = leitor["Email"].ToString();
                    funcionario.Telefone = leitor["Telefone"].ToString();

                    // adicionar o funcionario na lista de funcionario
                    funcionarios.Add(funcionario);
                }
            }
            catch (Exception ex)
            {

                msg = "Errp ap consultar os funcionarios cadrastrados " + ex.Message;
                titulo = "Erro ...";
                MessageBox.Show(msg,titulo, MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            finally
            {
                connection.Close();
            }
            return funcionarios;
        }

        public void Excluir(Funcionario funcionario)
        {
            // INSTRUÇÃO SQL
            sql = "DELETE FROM Funcionario WHERE IDFuncionario = @IDFuncionario";
            try
            {
                // ABRE A CONEXÃO COM O BANCO DE DADOS
                connection.Open();

                // CRIA UM COMANDO SQL
                SqlCommand cmd = new SqlCommand(sql, connection);

                // ADICIONAR VALOR AO PARAMETRO @IDFuncionario
                cmd.Parameters.AddWithValue("@IDFuncionario", funcionario.ID); // ADICIONAR COM VALOR

                // EXECUTA O COMANDO NO SQL NO BANCO DE DADOS
                cmd.ExecuteNonQuery();

                // MENSADEM DE FEEDBACK
                msg = "Funcionario " + funcionario.Nome + "excluido com sucesso!";

                // TITULO DA MENSAGEM 
                titulo = "Sucesso ...";

                MessageBox.Show(msg,titulo,MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            catch (SqlException ex)
            {
                // MENSAGEM DE FODBACK
                msg = "Erro ao excluir o funcionario !"+ex.Message;

                // TITULO DA MENSAGEM
                titulo = "Erro ...";

                MessageBox.Show(msg,titulo,MessageBoxButtons.OK,MessageBoxIcon.Error);  
            }
            finally
            {
                // FECHAR A CONEXÃO COM O BANCO DE DADOS
                connection.Close();
            }
        }

        public void Salvar(Funcionario funcionario)
        {

            // VERIFICAR SE O ID DO FUNCINARIO É DIFERENTE DE 0
            if (funcionario.ID != 0)
            {
                // UPDATE
                sql = "UPDATE Funcionario SET Nome=@Nome, Cpf=@Cpf,Rg=@Rg, Email=@Email, Telefone=@Telefone WHERE IDFuncionario = @IDFuncionario";
            }
            else
            {
                sql = "INSERT INTO Funcionario(Nome, Cpf,Rg,Email,Telefone) VALUES(@Nome,@Cpf,@Rg,@Email,@Telefone)";
            }

            try
            {
                
                // abre uma conexão com o banco de dados
                connection.Open();

                // comando sql
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.Parameters.AddWithValue(@"IDFuncionario", funcionario.ID);
                cmd.Parameters.AddWithValue("@Nome", funcionario.Nome);
                cmd.Parameters.AddWithValue("@Cpf", funcionario.Cpf);
                cmd.Parameters.AddWithValue("@Rg", funcionario.Rg);
                cmd.Parameters.AddWithValue("@Email", funcionario.Email);
                cmd.Parameters.AddWithValue("@Telefone", funcionario.Telefone);

                //Executa o INSERT
                cmd.ExecuteNonQuery();
                msg = "Funcionario " + funcionario.Nome + " salvo com sucesso!";
                titulo = "Sucesso ...";
                MessageBox.Show(msg,titulo,MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            catch (SqlException ex)
            {
                msg = "Erro ao salvar funcionario !"+ex.Message;
                titulo = "Erro ...";
                MessageBox.Show(msg,titulo,MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            finally
            {
                connection.Close(); 
            }
        }
    }
}
