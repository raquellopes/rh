﻿using Rh.Senai.RH.Dao;
using Rh.Senai.RH.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rh.Senai.RH.Forms
{
    public partial class DependenciaForm : Form
    {
        public DependenciaForm()
        {
            InitializeComponent();
        }

        private void DependenciaForm_Load(object sender, EventArgs e)
        {
            PreencherDados();
        }

        public void PreencherDados()
        {
            DependenciaDao dao = new DependenciaDao();
            dgvDependencia.DataSource = dao.Consultar();

            
            dgvDependencia.ClearSelection();

            
        } // FIM METODO

        public void LimparFormulario()
        {
            txtDependenciaID.Clear();
            txtDependenciaDescricao.Clear();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtDependenciaDescricao.Text))
            {
                MessageBox.Show("Preencha todos os dados!");
            }
            else
            {
                Dependencia dependencia = new Dependencia();

                dependencia.Descricao = txtDependenciaDescricao.Text;

                DependenciaDao dao = new DependenciaDao();
                dao.Salvar(dependencia);

                PreencherDados();
            }
            
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            Dependencia dependencia = new Dependencia();

            long id = 0;
            if (long.TryParse(txtDependenciaID.Text, out id))
            {
                dependencia.ID = id;
            }

            DependenciaDao dao = new DependenciaDao();

            DialogResult resposta = MessageBox.Show("Deseja realmente excluir este tipo de dependencia? ","Mensagem",MessageBoxButtons.YesNo,MessageBoxIcon.Question);

            if (resposta.Equals(DialogResult.Yes))
            {
                dao.Excluir(dependencia);
            }

            PreencherDados();     

        }

        private void dgvDependencia_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvDependencia.CurrentRow != null)
            {
                txtDependenciaID.Text = dgvDependencia.CurrentRow.Cells[0].Value.ToString();
                txtDependenciaDescricao.Text = dgvDependencia.CurrentRow.Cells[1].Value.ToString();
            }

        }
    }
}
