﻿using Rh.Senai.RH.Dao;
using Rh.Senai.RH.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rh.Senai.RH.Forms
{
    public partial class FuncionarioForm : Form
    {
        public FuncionarioForm()
        {
            InitializeComponent();
        }

        private void btnSalvarFuncionario_Click(object sender, EventArgs e)
        {
            // VALIDAÇÃO
            // VERIFICAR SE OS CAMPOS OBRIGATORIOS FORAM PREENCHIDOS

            if (string.IsNullOrEmpty(txtNomeFuncionario.Text)
                && string.IsNullOrEmpty(txtCpfFuncionario.Text)
                && string.IsNullOrEmpty(txtRgFuncionario.Text)
                && string.IsNullOrEmpty(txtEmailFuncionario.Text)
                && string.IsNullOrEmpty(txtTelefoneFuncionario.Text))
            {
                MessageBox.Show("Preencha todos os dados!");
            }
            else
            {

                // instancia um funcionário
                Funcionario funcionario = new Funcionario();

                // OBTEM O ID DO FUNCIONARIO
                // SE O TEXTBOX DE ID DE FUNCIONARIO NÃO ESTIVER VARIO
                // SE FOI PREENCHIDO O TEXTBOX DE IDFUNCIONARIO
                if (!string.IsNullOrEmpty(txtIDFuncionario.Text))
                {
                    // CRIA UM ID 
                    long id = 0;

                    // CONVERTE O TEXTO DO TEXTBOX PARA LONG
                    // ARMAZANADO NA VARIAVEL ID
                    if (long.TryParse(txtIDFuncionario.Text, out id))
                    {
                        // ATRIBUI O ID AO OBJETO FUNCIONARIO
                        funcionario.ID = id;
                    }
                }

                // atribuir dados ao funcionario
                funcionario.Nome = txtNomeFuncionario.Text;
                funcionario.Cpf = txtCpfFuncionario.Text;
                funcionario.Rg = txtRgFuncionario.Text;
                funcionario.Email = txtEmailFuncionario.Text;
                funcionario.Telefone = txtTelefoneFuncionario.Text;

                // instancia o dao de funcionario
                FuncionarioDao dao = new FuncionarioDao();

                // salva o funcionario no banco de dados
                dao.Salvar(funcionario);
                PreencherDados();

                LimparFormulario();
            }
        } // Fim do evento de click

        // método do programador    


        private void LimparFormulario()
        {
            txtIDFuncionario.Clear();
            txtNomeFuncionario.Clear();
            txtCpfFuncionario.Clear();
            txtRgFuncionario.Clear();
            txtEmailFuncionario.Clear();
            txtTelefoneFuncionario.Clear();
        }

        private void FuncionarioForm_Load(object sender, EventArgs e)
        {
            PreencherDados();
        }

        /// <summary>
        /// Métodos responsavel por preencher os dados
        /// </summary>
        private void PreencherDados()
        {
            FuncionarioDao dao = new FuncionarioDao();

            dgvFuncionarios.DataSource = dao.Consultar();

            dgvFuncionarios.Columns["ID"].Visible = false; // NÃO APARECER
            dgvFuncionarios.Columns["RG"].Visible = false; // NÃO APARECER

            // LIMPA A SELEÇÃO DO DATA GRID VIEW
            dgvFuncionarios.ClearSelection();

            // LIMPAR OS CAMPOS DO FORMULARIO
            LimparFormulario();
        }

        private void dgvFuncionarios_SelectionChanged(object sender, EventArgs e)
        {
            // se alguma linha for selecionada
            if(dgvFuncionarios.CurrentRow != null)
            {
                // pega o id e coloca bo textbox de id
                txtIDFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[0].Value.ToString();
                txtNomeFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[1].Value.ToString();
                txtCpfFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[2].Value.ToString();
                txtRgFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[3].Value.ToString();
                txtEmailFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[4].Value.ToString();
                txtTelefoneFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[5].Value.ToString();
            }
        }

        private void btnExcluirFuncionario_Click(object sender, EventArgs e)
        {
            // VERIFICAR SE O TEXTBOX DE ID FUNCIONARIO É NULO OU VAZIO
            // SE SIM ISSO SIGNIFICA QUE NENHUM FUNCIONARIO FOI SELECIONADO NA LISTA
            if (string.IsNullOrEmpty(txtIDFuncionario.Text)) // FUNÇÃO PARA SABER SE ESTA VAZIO
            {
                string msg = "Selecione um funcionario na lista abaixo !";
                string titulo = "Operação não realizada ...";

                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Warning); // WARNING ALERTA!
            }
            else
            {
                // INSTAMCIA UM FUNCIONARIO
                Funcionario funcionario = new Funcionario();

                //CRIA UM ID PARA O FUNCIONARIO
                // CONVERTER A STRING DO TEXT BOX DE ID PARA LONG
                long id = 0;
                if (long.TryParse(txtIDFuncionario.Text, out id))
                {
                    // ATRIBUI O ID AO ID FUNCIONARIO
                    funcionario.ID = id;
                }

                // INSTANCIA UMA DAO
                FuncionarioDao dao = new FuncionarioDao();

                // REPOSTA DO USUARIO
                DialogResult resposta = MessageBox.Show("Deseja realmente exlcuir este funcionario ?","Mensagem",MessageBoxButtons.YesNo,MessageBoxIcon.Question);

                // SE O URUARIO RESPONDER SIM
                if (resposta.Equals(DialogResult.Yes))
                {
                    // EXCLUIR FUNCIONARIO
                    dao.Excluir(funcionario);

                    // Atualiza dados
                    PreencherDados();
                }

            }
        }

        private void txtCpfFuncionario_Leave(object sender, EventArgs e)
        {
            // PEGA O TEXTO DO TEXTBOX DE CPF
            string cpf = txtCpfFuncionario.Text;

            // CRIA UM FUNCIONARIO 
            Funcionario funcionario = new FuncionarioDao().Consultar(cpf);

            // VERIFICAR SE O FUNCIONARIO NÃO É NULO
            if (funcionario != null)
            {
                txtIDFuncionario.Text = funcionario.ID.ToString();
                txtCpfFuncionario.Text = funcionario.Cpf;
                txtRgFuncionario.Text = funcionario.Rg;
                txtNomeFuncionario.Text = funcionario.Nome;
                txtEmailFuncionario.Text = funcionario.Email;
                txtTelefoneFuncionario.Text = funcionario.Telefone;
            }

        }

        private void txtNomeFuncionario_TextChanged(object sender, EventArgs e)
        {

        }
    } // FIM CLASSE
}
