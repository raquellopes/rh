﻿namespace Rh.Senai.RH.Forms
{
    partial class DependenciaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDependenciaID = new System.Windows.Forms.TextBox();
            this.txtDependenciaDescricao = new System.Windows.Forms.TextBox();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.dgvDependencia = new System.Windows.Forms.DataGridView();
            this.btnExcluir = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDependencia)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Descrição";
            // 
            // txtDependenciaID
            // 
            this.txtDependenciaID.Enabled = false;
            this.txtDependenciaID.Location = new System.Drawing.Point(16, 33);
            this.txtDependenciaID.Name = "txtDependenciaID";
            this.txtDependenciaID.Size = new System.Drawing.Size(100, 26);
            this.txtDependenciaID.TabIndex = 2;
            // 
            // txtDependenciaDescricao
            // 
            this.txtDependenciaDescricao.Location = new System.Drawing.Point(16, 92);
            this.txtDependenciaDescricao.Name = "txtDependenciaDescricao";
            this.txtDependenciaDescricao.Size = new System.Drawing.Size(334, 26);
            this.txtDependenciaDescricao.TabIndex = 3;
            // 
            // btnSalvar
            // 
            this.btnSalvar.Location = new System.Drawing.Point(275, 124);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(75, 32);
            this.btnSalvar.TabIndex = 4;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // dgvDependencia
            // 
            this.dgvDependencia.AllowUserToAddRows = false;
            this.dgvDependencia.AllowUserToDeleteRows = false;
            this.dgvDependencia.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDependencia.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDependencia.Location = new System.Drawing.Point(25, 213);
            this.dgvDependencia.Name = "dgvDependencia";
            this.dgvDependencia.ReadOnly = true;
            this.dgvDependencia.RowHeadersVisible = false;
            this.dgvDependencia.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDependencia.Size = new System.Drawing.Size(310, 142);
            this.dgvDependencia.TabIndex = 5;
            this.dgvDependencia.SelectionChanged += new System.EventHandler(this.dgvDependencia_SelectionChanged);
            // 
            // btnExcluir
            // 
            this.btnExcluir.Location = new System.Drawing.Point(194, 124);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(75, 32);
            this.btnExcluir.TabIndex = 6;
            this.btnExcluir.Text = "Excluir";
            this.btnExcluir.UseVisualStyleBackColor = true;
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // DependenciaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(368, 378);
            this.Controls.Add(this.btnExcluir);
            this.Controls.Add(this.dgvDependencia);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.txtDependenciaDescricao);
            this.Controls.Add(this.txtDependenciaID);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "DependenciaForm";
            this.Text = "DependenciaForm";
            this.Load += new System.EventHandler(this.DependenciaForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDependencia)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDependenciaID;
        private System.Windows.Forms.TextBox txtDependenciaDescricao;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.DataGridView dgvDependencia;
        private System.Windows.Forms.Button btnExcluir;
    }
}