﻿using Rh.Senai.RH.Dao;
using Rh.Senai.RH.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rh.Senai.RH.Forms
{
    public partial class DependenteForm : Form
    {
        public DependenteForm()
        {
            InitializeComponent();
        }

        private void DependenteForm_Load(object sender, EventArgs e)
        {
            // PREECHE COMBO BOX
            PreencheComboBox();

            // PREENCHE O DATA GRID VIEW
            PreencheDados();

            LimparFormulario();
        }

        // PREENCHE COMBOBOX
        private void PreencheComboBox()
        {
            // PREENCHE A COMBOBOX DE DEPENDENCIA
            // DEFINE A FONTE DE DADOS DA COMBOBOX
            cboDependencia.DataSource = new DependenciaDao().Consultar();
            cboDependencia.DisplayMember = "Descricao";

            cboFuncionario.DataSource = new FuncionarioDao().Consultar();
            cboFuncionario.DisplayMember = "Nome";
        }

        private void PreencheDados()
        {
            // DEFINE A FONTE DE DADOS DO DATA DRID VIEW
            dgvDependente.DataSource = new DependenteDao().Consultar();
        }

        private void LimparFormulario()
        {
            txtIDDependente.Clear();
            txtCPFDependente.Clear();
            dtpDataNascimento.Value = DateTime.Now;
            txtNomeDependente.Clear();
            PreencheComboBox();
            txtCPFDependente.Focus();
        }
        private void btnSalvar_Click(object sender, EventArgs e)
        {
            // CRIA UMA NOVA INSTANCIA DE DEPENDENTE
            Dependente dependente = new Dependente();

            if (!string.IsNullOrEmpty(txtIDDependente.Text))
            {
                long id = 0;
                if (long.TryParse(txtIDDependente.Text, out id))
                {
                    dependente.ID = id;
                }
            }

            //PREENCHE O DADOS DO DEPENDENTE
            dependente.Nome = txtNomeDependente.Text;
            dependente.Cpf = txtNomeDependente.Text;

            // pega a data do date time
            dependente.DataNascimento = dtpDataNascimento.Value;

            // OBTEM A DEPENDENCIA DO COMBOBOX
            Dependencia dependencia = (Dependencia) cboDependencia.SelectedItem;
            Funcionario funcionario = (Funcionario)cboFuncionario.SelectedItem;

            // ASSOCIA O DEPENDENTE A DEPENDENCIA
            dependente.Dependencia = dependencia;

            // ASSOCIA O DEPENDENTE AO FUNCIONARIO  
            dependente.Funcionario = funcionario;

            // CRIA UMA INSTANCIA DE DAO DEPENDENTE
            DependenteDao dao = new DependenteDao();

            // SALVA O DEPENDENTE 
            dao.Salvar(dependente);

            //ATUALIZAR O DATAGRID
            PreencheDados();
            LimparFormulario();
        }

        private void dgvDependente_SelectionChanged(object sender, EventArgs e)
        {
            // PREENCHE OS CAMPOS DO FORM 
            // COM OS DADOS DO ITEM SELECIONADO
            txtIDDependente.Text = dgvDependente.CurrentRow.Cells[0].Value.ToString();
            txtNomeDependente.Text = dgvDependente.CurrentRow.Cells[1].Value.ToString();
            txtCPFDependente.Text = dgvDependente.CurrentRow.Cells[2].Value.ToString();
            dtpDataNascimento.Value = (DateTime)dgvDependente.CurrentRow.Cells[3].Value;

            // DEFINE O ITEM DA COMBOBOX
            cboDependencia.Text = dgvDependente.CurrentRow.Cells[4].Value.ToString();
            cboFuncionario.Text = dgvDependente.CurrentRow.Cells[5].Value.ToString();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtIDDependente.Text))
            {
                string msg = "Selecione um funcionario";
                string titulo = "Operação não realizada";

                MessageBox.Show(msg, titulo, MessageBoxButtons.OK,MessageBoxIcon.Warning);
            }
            else
            {
                // INSTANCIAR DEPENDENTE
                Dependente dependente = new Dependente();

                //CRIA UM ID PARA O FUNCIONARIO
                // CONVERTER A STRING DO TEXT BOX DE ID PARA LONG
                long id = 0;
                if (long.TryParse(txtIDDependente.Text, out id))
                {
                    dependente.ID = id;
                }

                DependenteDao dao = new DependenteDao();

                DialogResult reposta = MessageBox.Show("Deseja realmente excluir esse dependente? ", "Mensagem", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (reposta.Equals(DialogResult.Yes))
                {
                    dao.Excluir(dependente);
                }
                PreencheDados();
                LimparFormulario();
            }  

        }
    }
}
