﻿namespace Rh.Senai.RH.Forms
{
    partial class DependenteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtIDDependente = new System.Windows.Forms.TextBox();
            this.txtNomeDependente = new System.Windows.Forms.TextBox();
            this.txtCPFDependente = new System.Windows.Forms.MaskedTextBox();
            this.dtpDataNascimento = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.cboFuncionario = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cboDependencia = new System.Windows.Forms.ComboBox();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.dgvDependente = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDependente)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(74, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(152, 141);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "CPF";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(361, 139);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(143, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "Data de Nascimento";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(148, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 18);
            this.label4.TabIndex = 3;
            this.label4.Text = "Nome";
            // 
            // txtIDDependente
            // 
            this.txtIDDependente.Enabled = false;
            this.txtIDDependente.Location = new System.Drawing.Point(77, 38);
            this.txtIDDependente.Name = "txtIDDependente";
            this.txtIDDependente.Size = new System.Drawing.Size(55, 24);
            this.txtIDDependente.TabIndex = 4;
            // 
            // txtNomeDependente
            // 
            this.txtNomeDependente.Location = new System.Drawing.Point(151, 38);
            this.txtNomeDependente.Name = "txtNomeDependente";
            this.txtNomeDependente.Size = new System.Drawing.Size(420, 24);
            this.txtNomeDependente.TabIndex = 5;
            // 
            // txtCPFDependente
            // 
            this.txtCPFDependente.Location = new System.Drawing.Point(155, 160);
            this.txtCPFDependente.Mask = "000,000,000-00";
            this.txtCPFDependente.Name = "txtCPFDependente";
            this.txtCPFDependente.Size = new System.Drawing.Size(197, 24);
            this.txtCPFDependente.TabIndex = 6;
            this.txtCPFDependente.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // dtpDataNascimento
            // 
            this.dtpDataNascimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDataNascimento.Location = new System.Drawing.Point(364, 160);
            this.dtpDataNascimento.Name = "dtpDataNascimento";
            this.dtpDataNascimento.Size = new System.Drawing.Size(197, 24);
            this.dtpDataNascimento.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(152, 87);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 18);
            this.label5.TabIndex = 8;
            this.label5.Text = "Funcionario";
            // 
            // cboFuncionario
            // 
            this.cboFuncionario.FormattingEnabled = true;
            this.cboFuncionario.Location = new System.Drawing.Point(155, 108);
            this.cboFuncionario.Name = "cboFuncionario";
            this.cboFuncionario.Size = new System.Drawing.Size(197, 26);
            this.cboFuncionario.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(371, 80);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 18);
            this.label6.TabIndex = 10;
            this.label6.Text = "Dependencia";
            // 
            // cboDependencia
            // 
            this.cboDependencia.FormattingEnabled = true;
            this.cboDependencia.Location = new System.Drawing.Point(364, 108);
            this.cboDependencia.Name = "cboDependencia";
            this.cboDependencia.Size = new System.Drawing.Size(197, 26);
            this.cboDependencia.TabIndex = 11;
            // 
            // btnSalvar
            // 
            this.btnSalvar.Location = new System.Drawing.Point(493, 202);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(81, 27);
            this.btnSalvar.TabIndex = 12;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // btnExcluir
            // 
            this.btnExcluir.Location = new System.Drawing.Point(406, 202);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(81, 27);
            this.btnExcluir.TabIndex = 13;
            this.btnExcluir.Text = "Excluir";
            this.btnExcluir.UseVisualStyleBackColor = true;
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // dgvDependente
            // 
            this.dgvDependente.AllowUserToAddRows = false;
            this.dgvDependente.AllowUserToDeleteRows = false;
            this.dgvDependente.AllowUserToResizeColumns = false;
            this.dgvDependente.AllowUserToResizeRows = false;
            this.dgvDependente.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDependente.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgvDependente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDependente.Location = new System.Drawing.Point(30, 260);
            this.dgvDependente.MultiSelect = false;
            this.dgvDependente.Name = "dgvDependente";
            this.dgvDependente.ReadOnly = true;
            this.dgvDependente.RowHeadersVisible = false;
            this.dgvDependente.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvDependente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDependente.Size = new System.Drawing.Size(617, 215);
            this.dgvDependente.TabIndex = 14;
            this.dgvDependente.SelectionChanged += new System.EventHandler(this.dgvDependente_SelectionChanged);
            // 
            // DependenteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(684, 506);
            this.Controls.Add(this.dgvDependente);
            this.Controls.Add(this.btnExcluir);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.cboDependencia);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cboFuncionario);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dtpDataNascimento);
            this.Controls.Add(this.txtCPFDependente);
            this.Controls.Add(this.txtNomeDependente);
            this.Controls.Add(this.txtIDDependente);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DependenteForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DependenteForm";
            this.Load += new System.EventHandler(this.DependenteForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDependente)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtIDDependente;
        private System.Windows.Forms.TextBox txtNomeDependente;
        private System.Windows.Forms.MaskedTextBox txtCPFDependente;
        private System.Windows.Forms.DateTimePicker dtpDataNascimento;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cboFuncionario;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cboDependencia;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.DataGridView dgvDependente;
    }
}