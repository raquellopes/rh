﻿using Rh.Senai.RH.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rh
{
    public partial class HomeForm : Form
    {
        public HomeForm()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            string mensagem = "Sistema de RH v1.0 \nDesenvolvido por: Raquel Lopes";
            string titulo = "Sobre ...";
            MessageBox.Show(mensagem,titulo, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void funcionárioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // abre o form de cadrastro de funcionarios

            // cria uma instancia do form
            FuncionarioForm form = new FuncionarioForm();

            // chama o método ShowDialog()
            form.ShowDialog(); // ShowDialog mostrar na tela
        }

        private void dependênciaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // JUNTA OS FORMULARIOS
            DependenciaForm form = new DependenciaForm();
            form.ShowDialog();
        }

        private void dependenteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // CRIA UMA INSTANCIA DE DEPENDENTEFORM
            DependenteForm form = new DependenteForm();

            // exibe o form
            form.ShowDialog();
        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Encerra a aplicação
            string msg = "Deseja realmente sair ?";
            string titulo = "Mensagem";
            DialogResult resposta = MessageBox.Show(msg,titulo,MessageBoxButtons.YesNo,MessageBoxIcon.Question);
            if (resposta.Equals(DialogResult.Yes))
            {
                Application.Exit();
            }
            else
            {

            }

        }

        private void funcionáriosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Relatorio form = new Relatorio();
            form.ShowDialog();
        }
    }
}
